CREATE SCHEMA `jdbc`;
use `jdbc`;

CREATE USER 'user_jdbc'@'localhost' IDENTIFIED BY 'jdbc01';

GRANT USAGE ON jdbc.* TO 'user_jdbc'@'localhost' identified BY 'jdbc01';
GRANT ALL PRIVILEGES ON jdbc.* TO 'user_jdbc'@'localhost';

FLUSH PRIVILEGES;

CREATE TABLE `jdbc`.`user`
(
  `id`        INT          NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname`  VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `jdbc`.`user` (firstname, lastname)
VALUES ('Jan', 'Kowalski'),
       ('Karol', 'Malinowski'),
       ('Anna', 'Perucka'),
       ('Tomasz', 'Walicki'),
       ('Katarzyna', 'Kozak'),
       ('Janusz', 'Bagiński'),
       ('Monika', 'Zakrzewska'),
       ('Tomasz', 'Kot');


CREATE TABLE `jdbc`.`task`
(
  `id`           INT         NOT NULL AUTO_INCREMENT,
  `title`        VARCHAR(45) NOT NULL,
  `priority`     INT         NOT NULL,
  `pinned`       BOOL        NOT NULL,
  `created_date` DATE        NOT NULL,
  `owner_id`     INT         NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `owner_id` FOREIGN KEY (`id`) REFERENCES `jdbc`.`user` (`id`)
);
