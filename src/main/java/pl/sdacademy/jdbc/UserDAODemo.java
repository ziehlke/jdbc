package pl.sdacademy.jdbc;

import pl.sdacademy.jdbc.dao.UserDAO;
import pl.sdacademy.jdbc.db.DBUtil;
import pl.sdacademy.jdbc.domain.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class UserDAODemo {

    public static void main(String[] args) throws SQLException {

        Connection conn = DBUtil.getConnection();
        UserDAO userDAO = new UserDAO(conn);

        Optional<User> one = userDAO.findOne(1);
        System.out.println(one.orElse(null));

        System.out.println(userDAO.findAll());

//        System.out.println(userDAO.findAllSortByFirstname());
//        System.out.println(userDAO.findAllSortByLastname());
//        System.out.println(userDAO.findAllNewestFirst());


        userDAO.modifyUser(1,"noweImie","noweNazwisko");
        System.out.println(userDAO.findAll());
        userDAO.removeUserByFirstname("noweImie");
        System.out.println(userDAO.findAll());


        DBUtil.closeConnection();
    }
}
