package pl.sdacademy.jdbc;

import pl.sdacademy.jdbc.db.DBUtil;
import pl.sdacademy.jdbc.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Users {

	public static void main(String[] args) throws SQLException {
		Connection conn = DBUtil.getConnection();

/*		List<User> userList = getUsers(conn);
		System.out.println(userList);
//		insertUser(conn, "Marian", "Stefański");
		userList = getUsers(conn);
		System.out.println(userList);*/


		System.out.println("------------------------------------");


		String query = "select * from user where id=?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setInt(1, 2);
		ResultSet result = statement.executeQuery();

		System.out.println(result.getInt("id"));
		System.out.println(result.getString("firstname"));
		System.out.println(result.getString("lastname"));


/*		Optional<User> user = Optional.of(new User(
				result.getInt("id"),
				result.getString("firstname"),
				result.getString("lastname")
		));

		System.out.println(user.get());*/



		DBUtil.closeConnection();

	}

	private static void insertUser(Connection conn, String firstname, String lastname) throws SQLException {
		String query = "INSERT INTO user (firstname, lastname) VALUE (?,?)";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, firstname);
		statement.setString(2, lastname);

		int result = statement.executeUpdate();
	}
/*

	private static List<User> getUsers(Connection conn) throws SQLException {
		Statement statement = conn.createStatement();
		String query = "select * from user";
		ResultSet result = statement.executeQuery(query);

		List<User> userList = new ArrayList<>();
		while (result.next()) {
			User user = new User(result.getInt("id"),
					result.getString("firstname"),
					result.getString("lastname")
			);
			userList.add(user);
		}
		result.close();
		statement.close();

		return userList;
	}
*/

	private static void deleteUser(Connection conn) throws SQLException {
		String query = "DELETE FROM user WHERE id = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, 5);
		int cnt = ps.executeUpdate();
		System.out.println("Usunieto " + cnt + " userow");
	}

}
