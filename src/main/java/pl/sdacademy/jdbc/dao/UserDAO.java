package pl.sdacademy.jdbc.dao;

import pl.sdacademy.jdbc.db.DBUtil;
import pl.sdacademy.jdbc.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAO {

    private final Connection conn;

    public UserDAO(Connection conn) {
        this.conn = conn;
    }

    public Optional<User> findOne(int id) {
        User user = new User();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = conn.prepareStatement("SELECT * FROM user WHERE id=?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user.setId(id);
                user.setFirstname(resultSet.getString("firstname"));
                user.setLastname(resultSet.getString("lastname"));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return Optional.of(user);

    }



/*    public Optional<User> findOne(int id) throws SQLException {
        String query = "select * from user where id=?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();

        return Optional.of(new User(
                result.getInt("id"),
                result.getString("firstname"),
                result.getString("lastname")
        ));
    }
*/

    public List<User> findAll() throws SQLException {
        Statement statement = conn.createStatement();
        String query = "select * from user";
        ResultSet result = statement.executeQuery(query);

        List<User> userList = new ArrayList<>();
        while (result.next()) {
            User user = new User(result.getInt("id"),
                    result.getString("firstname"),
                    result.getString("lastname")
            );
            userList.add(user);
        }
        result.close();
        statement.close();

        return userList;
    }

    public List<User> findAllSortByFirstname() throws SQLException {
        Statement statement = conn.createStatement();
        String query = "select * from user order by firstname";
        ResultSet result = statement.executeQuery(query);

        List<User> userList = new ArrayList<>();
        while (result.next()) {
            User user = new User(result.getInt("id"),
                    result.getString("firstname"),
                    result.getString("lastname")
            );
            userList.add(user);
        }
        result.close();
        statement.close();

        return userList;
    }

    public List<User> findAllSortByLastname() throws SQLException {
        Statement statement = conn.createStatement();
        String query = "select * from user order by lastname";
        ResultSet result = statement.executeQuery(query);

        List<User> userList = new ArrayList<>();
        while (result.next()) {
            User user = new User(result.getInt("id"),
                    result.getString("firstname"),
                    result.getString("lastname")
            );
            userList.add(user);
        }
        result.close();
        statement.close();

        return userList;
    }

    public List<User> findAllNewestFirst() throws SQLException {
        Statement statement = conn.createStatement();
        String query = "select * from user order by id desc";
        ResultSet result = statement.executeQuery(query);

        List<User> userList = new ArrayList<>();
        while (result.next()) {
            User user = new User(result.getInt("id"),
                    result.getString("firstname"),
                    result.getString("lastname")
            );
            userList.add(user);
        }
        result.close();
        statement.close();

        return userList;
    }


    public boolean createUser(String firstname, String lastname) {
        PreparedStatement statement = null;

        try {
            String query = "INSERT INTO user VALUES (?,?)";
            statement = conn.prepareStatement(query);
            statement.setString(1, firstname);
            statement.setString(1, lastname);
            statement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            return false;

        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

/*	public User createUser(String firstname, String lastname) {
		// TODO:
		// hint: https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-usagenotes-last-insert-id.html
		return null;
	}*/

    public void modifyUser(int id, String newFirstname, String newLastname) {
        PreparedStatement statement = null;

        try {
            String query = "UPDATE user SET firstname = ?, lastname = ? WHERE id = ?";

            statement = conn.prepareStatement(query);
            statement.setString(1, newFirstname);
            statement.setString(2, newLastname);
            statement.setInt(3, id);
            statement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void removeUserByFirstname(String firstname) {
        PreparedStatement statement = null;

        try {
            String query = "DELETE FROM user WHERE firstname = ?";

            statement = conn.prepareStatement(query);
            statement.setString(1, firstname);
            statement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
